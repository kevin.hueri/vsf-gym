// Config pour process.env
require("dotenv").config();

// Import des modules complémentaire
const express = require("express"),
  mongoose = require("mongoose"),
  app = express(),
  bodyParser = require('body-parser'),
  cors = require('cors'),
  port = process.env.PORT || 3008

app.use(cors())

// Config pour parser les corps de requête (formulaire : req.body)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// Express static permet de diriger un chemin sur un dossier en particulier
app.use("/assets", express.static("public"));

// Config de connexion à MongoDB
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Connecter a MongoDB"))
  .catch((err) => console.log(err));

// Import de notre router
const ROUTER = require('./router')
app.use('/api', ROUTER)

// Run de notre application (app)
// Lancement de l'application
app.listen(port, () => {
  console.log("le serveur tourne sur le port: " + port);
});
