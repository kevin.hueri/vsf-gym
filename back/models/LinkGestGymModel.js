/*
 *
 * Model de 'Link'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const LinkSchema = new Schema({
  link: {
    type: String,
    required: false
  }
});

module.exports = mongoose.model("Link", LinkSchema);