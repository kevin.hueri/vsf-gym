/*
 *
 * Model de 'Histoire du club'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const HistoireClubSchema = new Schema({
  histoire: {
    type: String,
    required: true
  },
});

module.exports = mongoose.model("HistoireClub", HistoireClubSchema);