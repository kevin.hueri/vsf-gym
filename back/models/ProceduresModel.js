/*
 *
 * Model de 'Inscription'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProceduresSchema = new Schema({
  procedure: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("Procedures", ProceduresSchema);