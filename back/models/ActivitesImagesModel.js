/*
 *
 * Model de 'Activités Images'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Import Model
const Activites = require('./ActivitesModel')

const ActivitesImagesSchema = new Schema({
  image: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: false
  },
  activitesId: {
    type: Schema.Types.ObjectId,
    ref: 'Activites'
  }
});

// Et l'on export notre model grace à la passerelle Mongoose
// Ce qui nous permettra de pouvoir l'utiliser sur d'autre page
module.exports = mongoose.model("ActivitesImages", ActivitesImagesSchema);