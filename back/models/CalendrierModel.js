/*
 *
 * Model de 'Calendrier'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Création de notre Shéma (Model)
// c'est le Model de (Model)
const CalendrierSchema = new Schema({
  // Première variable (basique)
  titre: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  dateDebut: {
    type: String,
    required: true
  },
  dateFin: {
    type: String,
    required: false
  }, 
});

// Et l'on export notre model grace à la passerelle Mongoose
// Ce qui nous permettra de pouvoir l'utiliser sur d'autre page
module.exports = mongoose.model("Calendrier", CalendrierSchema);