/*
 *
 * Model de 'Boutique'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BoutiqueSchema = new Schema({
    image: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: false
    },
    titre: {
        type: String,
        required: true
    },
    prix: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: false
    }
});

module.exports = mongoose.model("Boutique", BoutiqueSchema);