/*
 *
 * Model de 'Image Inscription'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ImageInscriptionSchema = new Schema({
  image: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: false
  },
});

module.exports = mongoose.model("ImageInscription", ImageInscriptionSchema);