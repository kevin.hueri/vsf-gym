/*
 *
 * Model de 'Salle'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SalleSchema = new Schema({
  image: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: false
  },
});

module.exports = mongoose.model("Salle", SalleSchema);