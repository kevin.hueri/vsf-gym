/*
 *
 * Model de 'Tarifs'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TarifsSchema = new Schema({
  activite: {
    type: String,
    required: true
  },
  prix: {
    type: Number,
    required: true
  },
});

module.exports = mongoose.model("Tarifs", TarifsSchema);