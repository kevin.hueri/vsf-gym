/*
 *
 * Model de 'Activités'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Import Model
const ActivitesImages = require('./ActivitesImagesModel');

// Création de notre Shéma (Model)
const ActivitesSchema = new Schema({
  titre: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  activitesImagesId: [{
    type: Schema.Types.ObjectId,
    ref: "ActivitesImages"
  }]
});

// Et l'on export notre model grace à la passerelle Mongoose
// Ce qui nous permettra de pouvoir l'utiliser sur d'autre page
module.exports = mongoose.model("Activites", ActivitesSchema);