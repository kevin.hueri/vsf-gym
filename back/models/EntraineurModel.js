/*
 *
 * Model de 'Entraineur'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const EntraineurSchema = new Schema({
  nom: {
    type: String,
    required: true
  },
  prenom: {
    type: String,
    required: true
  },
  diplome: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("Entraineur", EntraineurSchema);