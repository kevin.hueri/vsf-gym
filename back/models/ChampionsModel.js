/*
 *
 * Model de 'Champions'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ChampionsSchema = new Schema({
  image: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: false
  }
});

module.exports = mongoose.model("Champions", ChampionsSchema);