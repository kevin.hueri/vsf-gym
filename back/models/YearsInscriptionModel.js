/*
 *
 * Model de 'Years inscription'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const YearsSchema = new Schema({
  annee: {
    type: String,
    required: true
  },
});

module.exports = mongoose.model("Years", YearsSchema);