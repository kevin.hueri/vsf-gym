/*
 *
 * Model de 'Juges'
 ******************************/

// Import de Mongoose
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const JugesSchema = new Schema({
  nom: {
    type: String,
    required: true
  },
  prenom: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("Juges", JugesSchema);