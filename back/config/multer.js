// Import de Multer
const multer = require('multer');

const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        const ext = file.originalname,
            date = Date.now(),
            completed = date + '_' + ext;
        file.completed = completed;
        cb(null, completed)
    },
})

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 35 * 1024 * 1024, //ici limite la taille à 8 Moctet
        files: 1
    },
    fileFilter: (req, file, cb) => {
        if (
            file.mimetype === "image/png" ||
            file.mimetype === "image/jpg" ||
            file.mimetype === "image/gif" ||
            file.mimetype === "image/webp" ||
            file.mimetype === "image/jpeg"
        ) {
            cb(null, true)
        } else {
            cb(null, false)
        }
    }
})

module.exports = upload