/*
 * Activités Controllers
 * ****************** */

// Import modules
const func = require("../utils/function"),
    path = require("path"),
    fs = require('fs')

// Import Model
const Activites = require("../models/ActivitesModel");
const ActivitesImages = require("../models/ActivitesImagesModel")

// GetAll
exports.getAllImages = async (req, res) => {
    const dbActivitesImages = await ActivitesImages.find();
    res.json({
        message: "Voici vos images activites",
        dbActivitesImages
    });
};

// PostImages
exports.postImages = async (req, res) => {
    if (req.file) {
        const query = { _id: req.params.id }
        const activites = await Activites.findById(query)
        const fileActivites = req.file.completed;
        const pathImgWebp = fileActivites.split(".").slice(0, -1).join(".") + ".webp"
        const activitesImages = new ActivitesImages({
            image: `/assets/images/activities/${pathImgWebp}`,
            name: req.file.completed,
            activitesId: activites._id
        })
        activites.activitesImagesId.push(activitesImages._id)
        activitesImages.save((err) => { });
        activites.save((err) => { });
        const dbActivitesImages = await ActivitesImages.find();
        res.json({
            message: "Activité image créé avec succes !",
            dbActivitesImages
        });
    } else res.json({ message: "Erreur, l'image de l'activité n'as pas été créé!" });
}

// DeleteImages
exports.deleteImages = async (req, res) => {
    const ActivitesId = await ActivitesImages.findById(req.params.id);
    pathImg = path.resolve("public/images/activities/" + ActivitesId.name)
    await ActivitesImages.findByIdAndDelete(ActivitesId);
    const dbActivitesImages = await ActivitesImages.find();
    res.json({
        message: "L'image de l'activité a été supprimé avec succes !",
        dbActivitesImages
    });
    ActivitesName = ActivitesId.name;
    const pathImgWebp = ActivitesName.split(".").slice(0, -1).join(".") + ".webp";
    pathImgDelete = path.resolve("public/images/activities/" + pathImgWebp);
    fs.unlink(pathImgDelete, (err) => {
        if (err) console.log(err)
    })
}

// DeleteAll
exports.deleteAllImages = async (req, res) => {
    await ActivitesImages.find();
    await ActivitesImages.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes!" });
};