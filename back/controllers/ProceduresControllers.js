/*
 * Inscription Controllers
 * ****************** */

// Import Model
const Procedures = require("../models/ProceduresModel");


// GetAll
exports.getAll = async (req, res) => {
    const dbProcedures = await Procedures.find();
    res.json({
        message: "Voici vos procedures",
        dbProcedures
    });
};

// GetOne
exports.getId = async (req, res) => {
    const proceduresID = await Procedures.findById(req.params.id);
    res.json({
        message: "Voici votre Procedures ID",
        proceduresID
    });
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (b.addDocument) {
        const procedures = new Procedures({
            procedure: b.addDocument,
        });
        procedures.save((err) => { });
        const dbProcedures = await Procedures.find();
        res.json({
            message: "Procedures créé avec succes !",
            dbProcedures
        });
    } else res.json({ message: "Erreur, la Procedures n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    await Procedures.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbProcedures = await Procedures.find();
    res.json({
        message: "Procedures edité avec succes !",
        dbProcedures
    });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const ProceduresId = await Procedures.findById(req.params.id);
    await Procedures.findByIdAndDelete(ProceduresId);
    const dbProcedures = await Procedures.find();
    res.json({
        message: "La Procedures a été supprimé avec succes !",
        dbProcedures
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Procedures.find();
    await Procedures.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes!" });
};