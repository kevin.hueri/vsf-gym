/*
 * Champions Controllers
 * ****************** */

// Import Model
const EquipeAdministrative = require("../models/EquipeAdministrativeModel");


// GetAll
exports.getAll = async (req, res) => {
    const dbEquipeAdministrative = await EquipeAdministrative.find();
    res.json({
        message: "Voici vos equipe administrative",
        dbEquipeAdministrative
    });
};

// GetOne
exports.getId = async (req, res) => {
    const equipeAdministrativeID = await EquipeAdministrative.findById(req.params.id);
    res.json({
        message: "Voici votre equipe administrative ID",
        equipeAdministrativeID
    });
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
        if (b.nom && b.prenom && b.poste) {
            const equipeAdministrative = new EquipeAdministrative({
                nom: b.nom,
                prenom: b.prenom,
                poste: b.poste,
            });
            equipeAdministrative.save((err) => { });
            const dbEquipeAdministrative = await EquipeAdministrative.find();
            res.json({
                message: "Equipe Administrative créé avec succes !",
                dbEquipeAdministrative
            });
        } else res.json({ message: "Erreur, l'équipe Administrative n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    await EquipeAdministrative.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbEquipeAdministrative = await EquipeAdministrative.find();
    res.json({
        message: "L'équipe administrative edité avec succes !",
        dbEquipeAdministrative
    });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const EquipeAdministrativeId = await EquipeAdministrative.findById(req.params.id);
    await EquipeAdministrative.findByIdAndDelete(EquipeAdministrativeId);
    const dbEquipeAdministrative = await EquipeAdministrative.find();
    res.json({
        message: "Le Equipe Administrative a été supprimé avec succes !",
        dbEquipeAdministrative
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await EquipeAdministrative.find();
    await EquipeAdministrative.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes!" });
};