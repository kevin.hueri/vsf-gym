/*
 * Boutique Controllers
 * ****************** */

// Import modules
const func = require("../utils/function"),
    path = require("path"),
    fs = require('fs')

// Import Model
const Boutique = require("../models/BoutiqueModel");

// GetAll
exports.getAll = async (req, res) => {
    const dbBoutique = await Boutique.find();
    res.json({
        message: "Voici vos articles",
        dbBoutique
    });
};

// GetOne
exports.getId = async (req, res) => {
    const BoutiqueID = await Boutique.findById(req.params.id);
    res.json({
        message: "Voici votre article ID",
        BoutiqueID
    });
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (req.file) {
        const fileStore = req.file.completed;
        const pathImgWebp = fileStore.split(".").slice(0, -1).join(".") + ".webp"
        if (b.titre && b.prix) {
            const boutique = new Boutique({
                titre: b.titre,
                prix: b.prix,
                desc: b.desc,
                image: `/assets/images/boutique/${pathImgWebp}`,
                name: req.file.completed
            });
            boutique.save((err) => { });
            const dbBoutique = await Boutique.find();
            res.json({
                message: "Article créé avec succes !",
                dbBoutique
            });
        } else res.json({
            message: "Erreur, l'entraineur n'as pas été créé !"
        });
    } else res.json({ message: "Erreur, l'article' n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    const boutiqueID = await Boutique.findById(req.params.id);
    BoutiqueName = boutiqueID.name;
    const pathImgWebp = BoutiqueName.split(".").slice(0, -1).join(".") + ".webp";
    pathImgDelete = path.resolve("public/images/boutique/" + pathImgWebp);
    if (!req.file) {
        if ({ ...req.body }) {
            await Boutique.findByIdAndUpdate(req.params.id, { ...req.body })
            const dbBoutique = await Boutique.find();
            res.json({
                message: "Article edité avec succes !",
                dbBoutique
            });
        }
    } else {
        const fileStore = req.file.completed;
        const pathImgWebp = fileStore.split(".").slice(0, -1).join(".") + ".webp"
        await Boutique.findByIdAndUpdate(req.params.id, {
            titre: req.body.titre,
            prix: req.body.prix,
            desc: req.body.desc,
            image: `/assets/images/boutique/${pathImgWebp}`,
            name: req.file.completed
        })
        const dbBoutique = await Boutique.find();
        res.json({
            message: "Article edité avec succes !",
            dbBoutique
        });
        fs.unlink(pathImgDelete, (err) => { if (err) console.log(err) })
    }
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const boutiqueId = await Boutique.findById(req.params.id);
    BoutiqueName = boutiqueId.name;
    const pathImgWebp = BoutiqueName.split(".").slice(0, -1).join(".") + ".webp";
    pathImgDelete = path.resolve("public/images/boutique/" + pathImgWebp);
    await Boutique.findByIdAndDelete(boutiqueId);
    const dbBoutique = await Boutique.find();
    res.json({
        message: "L'article a été supprimé avec succes !",
        dbBoutique
    });
    fs.unlink(pathImgDelete, (err) => {
        if (err) console.log(err)
    })
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Boutique.find();
    await Boutique.deleteMany();
    res.json({
        message: "Tous les items ont été supprimés avec succes !"
    });
};
