/*
 * Champions Controllers
 * ****************** */

// Import modules
const func = require("../utils/function"),
    path = require("path"),
    fs = require('fs')

// Import Model
const Champions = require("../models/ChampionsModel");

// GetAll
exports.getAll = async (req, res) => {
    const dbChampions = await Champions.find();
    res.json({
        message: "Voici vos champions",
        dbChampions
    });
};

// GetOne
exports.getId = async (req, res) => {
    const championsID = await Champions.findById(req.params.id);
    res.json({
        message: "Voici votre Champion ID",
        championsID
    });
}

// Create
exports.post = async (req, res) => {
    if (req.file) {
        const fileChampions = req.file.completed;
        const pathImgWebp = fileChampions.split(".").slice(0, -1).join(".") + ".webp"
        const champions = new Champions({
            image: `/assets/images/champions/${pathImgWebp}`,
            name: req.file.completed
        });
        champions.save((err) => { });
        const dbChampions = await Champions.find();
        res.json({
            message: "champion créé avec succes !",
            dbChampions
        });
    } else res.json({ message: "Erreur, le champion n'as pas été créé !" });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const ChampionsId = await Champions.findById(req.params.id);
    pathImg = path.resolve("public/images/champions/" + ChampionsId.name);
    await Champions.findByIdAndDelete(ChampionsId);
    const dbChampions = await Champions.find();
    res.json({
        message: "Le champion va été supprimé avec succes !",
        dbChampions
    });
    ChampionsName = ChampionsId.name;
    const pathImgWebp = ChampionsName.split(".").slice(0, -1).join(".") + ".webp";
    pathImgDelete = path.resolve("public/images/champions/" + pathImgWebp);
    fs.unlink(pathImgDelete, (err) => { if (err) console.log(err) });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Champions.find();
    await Champions.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes !" });
};