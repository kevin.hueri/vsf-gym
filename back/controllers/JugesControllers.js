/*
 * Juges Controllers
 * ****************** */

// Import Model
const Juges = require("../models/JugesModel");


// GetAll
exports.getAll = async (req, res) => {
    const dbJuges = await Juges.find();
    res.json({
        message: "Voici vos Juges",
        dbJuges
    });
};

// GetOne
exports.getId = async (req, res) => {
    const jugesID = await Juges.findById(req.params.id);
    res.json({
        message: "Voici votre Juge ID",
        jugesID
    });
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (b.nom && b.prenom) {
        const juges = new Juges({
            nom: b.nom,
            prenom: b.prenom,
        });
        juges.save((err) => { });
        const dbJuges = await Juges.find();
        res.json({
            message: "Juge créé avec succes !",
            dbJuges
        });
    } else res.json({ message: "Erreur, le Juge n'as pas été créé!" });
};

// EditOne
exports.put = async (req, res) => {
    await Juges.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbJuges = await Juges.find();
    res.json({
        message: "Juges edité avec succes !",
        dbJuges
    });
}

// DeleteOne
exports.deleteOne = async (req, res) => {
    const JugesId = await Juges.findById(req.params.id);
    await Juges.findByIdAndDelete(JugesId);
    const dbJuges = await Juges.find();
    res.json({
        message: "Le Juge a été supprimé avec succes !",
        dbJuges
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Juges.find();
    await Juges.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes !" });
};