/*
 * ImageInscription Controllers
 * ****************** */

// Import modules
const func = require("../utils/function"),
    path = require("path"),
    fs = require('fs')

// Import Model
const ImageInscription = require("../models/ImageInscriptionModel");

// GetAll
exports.getAll = async (req, res) => {
    const dbImageInscription = await ImageInscription.find();
    res.json({
        message: "Voici vos images",
        dbImageInscription
    });
};

// GetOne
exports.getId = async (req, res) => {
    const imageInscriptionID = await ImageInscription.findById(req.params.id);
    res.json({
        message: "Voici votre image ID",
        imageInscriptionID
    });
}

// Create
exports.post = async (req, res) => {
    if (req.file) {
        const fileImage = req.file.completed;
        const pathImgWebp = fileImage.split(".").slice(0, -1).join(".") + ".webp"
        const imageInscription = new ImageInscription({
            image: `/assets/images/gallery/${pathImgWebp}`,
            name: req.file.completed
        });
        imageInscription.save((err) => { });
        const dbImageInscription = await ImageInscription.find();
        res.json({
            message: "Image créé avec succes !",
            dbImageInscription
        });
    } else res.json({ message: "Erreur, l'image n'as pas été créé !" });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const ImageId = await ImageInscription.findById(req.params.id);
    pathImg = path.resolve("public/images/gallery/" + ImageId.name)
    await ImageInscription.findByIdAndDelete(ImageId);
    const dbImageInscription = await ImageInscription.find();
    res.json({
      message: "L'image' a été supprimé avec succes !",
      dbImageInscription
    });
    ImageName = ImageId.name;
    const pathImgWebp = ImageName.split(".").slice(0, -1).join(".") + ".webp";
    pathImgDelete = path.resolve("public/images/gallery/" + pathImgWebp);
    fs.unlink(pathImgDelete, (err) => { if (err) console.log(err) })
  }