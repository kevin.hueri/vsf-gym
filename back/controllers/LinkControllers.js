/*
 * Link Controllers
 * ****************** */

// Import Model
const Link = require("../models/LinkGestGymModel");


// GetAll
exports.getAll = async (req, res) => {
    const dbLink = await Link.find();
    res.json({
        message: "Voici votre Link",
        dbLink
    });
};

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (b.link) {
        const link = new Link({ link: b.link });
        link.save((err) => { });
        const dbLink = await Link.find();
        res.json({
            message: "Link créé avec succes !",
            dbLink
        });
    } else res.json({ message: "Erreur, la Link n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    await Link.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbLink = await Link.find();
    res.json({
        message: "Link edité avec succes !",
        dbLink
    });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const LinkId = await Link.findById(req.params.id);
    await Link.findByIdAndDelete(LinkId);
    const dbLink = await Link.find();
    res.json({
        message: "La Link a été supprimé avec succes !",
        dbLink
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Link.find();
    await Link.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes !" });
};