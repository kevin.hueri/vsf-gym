/*
 * Calendrier Controllers
 * ****************** */

// Import Model
const Calendrier = require("../models/CalendrierModel");


// GetAll
exports.getAll = async (req, res) => {
    const dbCalendrier = await Calendrier.find();
    res.json({
        message: "Voici vos activites",
        dbCalendrier
    });
};

// GetOne
exports.getId = async (req, res) => {
    const calendrierID = await Calendrier.findById(req.params.id);
    res.json({
        message: "Voici votre Calendrier ID",
        calendrierID
    });
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (b.titre && b.type && b.dateDebut) {
        const calendrier = new Calendrier({
            titre: b.titre,
            type: b.type,
            dateDebut: b.dateDebut,
            dateFin: b.dateFin
        });
        calendrier.save((err) => { });
        const dbCalendrier = await Calendrier.find();
        res.json({
            message: "le calendrier créé avec succes !",
            dbCalendrier
        });
    } else res.json({ message: "Erreur, le calendrier n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    await Calendrier.findByIdAndUpdate(req.params.id, {...req.body})
    const dbCalendrier = await Calendrier.find();
    res.json({
        message: "Calendrier edité avec succes !",
        dbCalendrier
    });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const CalendrierId = await Calendrier.findById(req.params.id);
    await Calendrier.findByIdAndDelete(CalendrierId);
    const dbCalendrier = await Calendrier.find();
    res.json({
        message: "Le calendrier a été supprimé avec succes !",
        dbCalendrier
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Calendrier.find();
    await Calendrier.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes !" });
};