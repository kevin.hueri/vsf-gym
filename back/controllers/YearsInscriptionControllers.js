/*
 * Years Controllers
 * ****************** */

// Import Model
const Years = require("../models/YearsInscriptionModel");

// GetAll
exports.getAll = async (req, res) => {
    const dbYears = await Years.find();
    res.json({
        message: "Voici vos années",
        dbYears
    });
};

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (b.annee) {
        const years = new Years({ annee: b.annee });
        years.save((err) => { });
        const dbYears = await Years.find();
        res.json({
            message: "année crée avec succes !",
            dbYears
        });
    } else res.json({ message: "Erreur, l'année n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    await Years.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbYears = await Years.find();
    res.json({
        message: "année edité avec succes !",
        dbYears
    });
};