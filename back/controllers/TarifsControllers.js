/*
 * Tarifs Controllers
 * ****************** */

// Import Model
const Tarifs = require("../models/TarifsModel");

// GetAll
exports.getAll = async (req, res) => {
    const dbTarifs = await Tarifs.find();
    res.json({
        message: "Voici vos Tarifs",
        dbTarifs
    });
};

// GetOne
exports.getId = async (req, res) => {
    const tarifsID = await Tarifs.findById(req.params.id);
    res.json({
        message: "Voici votre tarifs ID",
        tarifsID
    });
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (b.activite && b.prix) {
        const tarifs = new Tarifs({
            activite: b.activite,
            prix: b.prix
        });
        tarifs.save((err) => { });
        const dbTarifs = await Tarifs.find();
        res.json({
            message: "Tarifs crée avec succes !",
            dbTarifs
        });
    } else res.json({ message: "Erreur, la Tarifs n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    await Tarifs.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbTarifs = await Tarifs.find();
    res.json({
        message: "Tarifs edité avec succes !",
        dbTarifs
    });
};


// DeleteOne
exports.deleteOne = async (req, res) => {
    const TarifsId = await Tarifs.findById(req.params.id);
    await Tarifs.findByIdAndDelete(TarifsId);
    const dbTarifs = await Tarifs.find();
    res.json({
        message: "Le Tarifs a été supprimé avec succes !",
        dbTarifs
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Tarifs.find();
    await Tarifs.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes !" });
};

