/*
 * Histoire club Controllers
 * ****************** */

// Import Model
const HistoireClub = require("../models/HistoireClubModel");


// GetAll
exports.getAll = async (req, res) => {
    const dbHistoireClub = await HistoireClub.find();
    res.json({
        message: "Voici votre histoire",
        dbHistoireClub
    });
};

// GetOne
exports.getId = async (req, res) => {
    const HistoireClubID = await HistoireClub.findById(req.params.id);
    res.json({
        message: "Voici votre histoire ID",
        HistoireClubID
    });
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (b.histoire) {
        const histoireClub = new HistoireClub({ histoire: b.histoire, });
        histoireClub.save((err) => { });
        const dbHistoireClub = await HistoireClub.find();
        res.json({
            message: "histoire créée avec succes !",
            dbHistoireClub
        });
    } else res.json({ message: "Erreur, l'histoire' n'as pas été créée!" });
};

// EditOne
exports.put = async (req, res) => {
    await HistoireClub.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbHistoireClub = await HistoireClub.find();
    res.json({
        message: "histoire editée avec succes !",
        dbHistoireClub
    });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const HistoireClubId = await HistoireClub.findById(req.params.id);
    await HistoireClub.findByIdAndDelete(HistoireClubId);
    const dbHistoireClub = await HistoireClub.find();
    res.json({
        message: "L'histoire a été supprimée avec succes !",
        dbHistoireClub
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await HistoireClub.find();
    await HistoireClub.deleteMany();
    res.json({ message: "Toutes les histoires ont été supprimées avec succes !" });
};
