/*
 * Activités Controllers
 * ****************** */

// Import Model
const Activites = require("../models/ActivitesModel");
const ActivitesImages = require("../models/ActivitesImagesModel")

// GetAll
exports.getAll = async (req, res) => {
    const dbActivites = await Activites
        .find()
        .populate("activitesImagesId")
        .then(dbActivites => {
            res.json({
                message: "Voici vos activites",
                dbActivites
            });
        })
};

// GetOne
exports.getId = async (req, res) => {
    const query = req.params.id;
    Activites
        .findById(query)
        .populate("activitesImagesId")
        .then(activites => { res.json(activites) })
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
    if (b.titre && b.description) {
        const activites = new Activites({
            titre: b.titre,
            description: b.description,
        });
        activites.save((err) => { });
        const dbActivites = await Activites.find();
        res.json({
            message: "activité créé avec succes !",
            dbActivites
        });
    } else res.json({ message: "Erreur, l'activité n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    await Activites.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbActivites = await Activites.find();
    res.json({
        message: "Activites edité avec succes !",
        dbActivites
    });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    const ActivitesId = await Activites.findById(req.params.id);
    await Activites.findByIdAndDelete(ActivitesId);
    const dbActivites = await Activites.find();
    res.json({
        message: "L'activité a été supprimé avec succes !",
        dbActivites
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Activites.find();
    await Activites.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes!" });
};