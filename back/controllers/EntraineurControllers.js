/*
 * Entraineur Controllers
 * ****************** */

// Import Model
const Entraineur = require("../models/EntraineurModel");

// GetAll
exports.getAll = async (req, res) => {
    const dbEntraineur = await Entraineur.find();
    res.json({
        message: "Voici vos entraineurs",
        dbEntraineur
    });
};

// GetOne
exports.getId = async (req, res) => {
    const entraineurID = await Entraineur.findById(req.params.id);
    res.json({
        message: "Voici votre Entraineur ID",
        entraineurID
    });
}

// Create
exports.post = async (req, res) => {
    const b = req.body;
        if (b.nom && b.prenom && b.diplome) {
            const entraineur = new Entraineur({
                nom: b.nom,
                prenom: b.prenom,
                diplome: b.diplome,
            });
            entraineur.save((err) => { });
            const dbEntraineur = await Entraineur.find();
            res.json({
                message: "Entraineur créée avec succes !",
                dbEntraineur
            });
        } else res.json({ message: "Erreur, l'entraineur n'as pas été créé !" });
};

// EditOne
exports.put = async (req, res) => {
    await Entraineur.findByIdAndUpdate(req.params.id, { ...req.body })
    const dbEntraineur = await Entraineur.find();
    res.json({
        message: "Entraineur edité avec succes !",
        dbEntraineur
    });
};


// DeleteOne
exports.deleteOne = async (req, res) => {
    const EntraineurId = await Entraineur.findById(req.params.id);
    await Entraineur.findByIdAndDelete(EntraineurId);
    const dbEntraineur = await Entraineur.find();
    res.json({
        message: "L'entraineur a été supprimé avec succes !",
        dbEntraineur
    });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Entraineur.find();
    await Entraineur.deleteMany();
    res.json({ message: "Tous les items ont été supprimés avec succes!" });
};