/*
 * Salle Controllers
 * ****************** */

// Import modules
const func = require("../utils/function"),
  path = require("path"),
  fs = require('fs')

// Import Model
const Salle = require("../models/SalleModel");

// GetAll
exports.getAll = async (req, res) => {
  const dbSalle = await Salle.find();
  res.json({
    message: "Voici vos Salle",
    dbSalle
  });
};

// GetOne
exports.getId = async (req, res) => {
  const salleID = await Salle.findById(req.params.id);
  res.json({
    message: "Voici votre Salle ID",
    salleID
  });
}

// Create
exports.post = async (req, res) => {
  if (req.file) {
    const fileSalle = req.file.completed;
    const pathImgWebp = fileSalle.split(".").slice(0, -1).join(".") + ".webp"
    const salle = new Salle({
      image: `/assets/images/gallery/${pathImgWebp}`,
      name: req.file.completed
    });
    salle.save((err) => { });
    const dbSalle = await Salle.find();
    res.json({
      message: "Salle créé avec succes !",
      dbSalle
    });
  } else res.json({ message: "Erreur, la salle n'as pas été créé !" });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
  const SalleId = await Salle.findById(req.params.id);
  pathImg = path.resolve("public/images/gallery/" + SalleId.name)
  await Salle.findByIdAndDelete(SalleId);
  const dbSalle = await Salle.find();
  res.json({
    message: "La Salle a été supprimé avec succes !",
    dbSalle
  });
  SalleName = SalleId.name;
  const pathImgWebp = SalleName.split(".").slice(0, -1).join(".") + ".webp";
  pathImgDelete = path.resolve("public/images/gallery/" + pathImgWebp);
  fs.unlink(pathImgDelete, (err) => { if (err) console.log(err) })
}

// DeleteAll
exports.deleteAll = async (req, res) => {
  await Salle.find();
  await Salle.deleteMany();
  res.json({ message: "Tous les items ont été supprimés avec succes !" });
};