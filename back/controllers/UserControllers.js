/*
 * User Controllers
 * ****************** */

// Import Model
const User = require("../models/UserModel");

// Import modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();

// GetAll
exports.getAll = async (req, res) => {
  const dbUser = await User.find();
  res.json({
    message: "Voici vos users",
    dbUser
  });
};

// GetOne
exports.getId = async (req, res) => {
  const userID = await User.findById(req.params.id);
  res.json({
    message: "Voici votre User ID",
    userID
  });
}

// Create
exports.create = async (req, res) => {
  const b = req.body;
  if (b.name && b.email && b.password) {
    const hash = await bcrypt.hash(b.password, 10);
    const user = new User({
      name: b.name,
      email: b.email,
      password: hash
    });
    user.save((err) => { });
    const dbUser = await User.find();
    res.json({
      message: "User crée avec succes !",
      dbUser
    });
  } else res.json({ message: "Erreur, l'user n'as pas été créé !" });
};

// EditOne
exports.editOne = async (req, res) => {
  await User.findByIdAndUpdate(req.params.id, { ...req.body })
  const dbUser = await User.find();
  res.json({
    message: "User edité avec succes !",
    dbUser
  });
};


// DeleteOne
exports.deleteOne = async (req, res) => {
  const UserId = await User.findById(req.params.id);
  await User.findByIdAndDelete(UserId);
  const dbUser = await User.find();
  res.json({
    message: "L'user a été supprimé avec succes !",
    dbUser
  });
}

// DeleteAll
exports.deleteAll = async (req, res) => {
  await User.find();
  await User.deleteMany();
  res.json({ message: "Tous les items ont été supprimés avec succes!" });
};

// Login
exports.login = async (req, res) => {
  const email = req.body.mail;
  const pass = req.body.password;
  User.findOne({ email }, (error, User) => {
    if (error) throw error;
    if (User) {
      bcrypt.compare(pass, User.password, (error, check) => {
        if (error) throw error;
        if (check) {
          let token = 'visitor';
          token = jwt.sign(
            {
              _id: check._id,
              mail: check.mail,
            },
            process.env.SIGN_JWT,
            { expiresIn: "1h" }
          );
          return res.status(200).send({
            success: 'success',
            token,
          });
        } else { res.json({ message: "Le mot de passe est incorrect" }) }
      })
    } else { res.json({ message: "Le mail n'existe pas" }) }
  })
}