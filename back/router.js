/*
 * Import Module
 * ************* */

const express = require('express'),
  router = express.Router(),
  sharpActivities = require("./config/sharpActivities"),
  sharpChampions = require("./config/sharpChampions"),
  sharpSalle = require("./config/sharpGallery"),
  sharpBoutique = require("./config/sharpBoutique"),
  upload = require('./config/multer')

// Controllers
const UserControllers = require("./controllers/UserControllers");
const EntraineurControllers = require('./controllers/EntraineurControllers');
const ActivitesControllers = require('./controllers/ActivitesControllers');
const ActivitesImagesControllers = require('./controllers/ActivitesImagesControllers');
const CalendrierControllers = require('./controllers/CalendrierController');
const ChampionsControllers = require('./controllers/ChampionsController');
const EquipeAdministrative = require('./controllers/EquipeAdministrativeController');
const ProceduresControllers = require('./controllers/ProceduresControllers');
const JugesControllers = require('./controllers/JugesControllers');
const SalleControllers = require('./controllers/SalleControllers');
const TarifsControllers = require('./controllers/TarifsControllers');
const YearsControllers = require('./controllers/YearsInscriptionControllers');
const LinkControllers = require('./controllers/LinkControllers');
const HistoireClubControllers = require('./controllers/HistoireClubControllers');
const ImageInscriptionControllers = require('./controllers/ImageInscriptionController');
const BoutiqueControllers = require('./controllers/BoutiqueControllers');

// Middlewares
// const MDTest = require('./test')

// Routes User
router
  .get("/user", UserControllers.getAll)
  .get("/user/:id", UserControllers.getId)
  .post("/user", UserControllers.create)
  .post("/login", UserControllers.login)
  .put('/user/:id', UserControllers.editOne)
  .delete('/user/:id', UserControllers.deleteOne)
  .delete('/user', UserControllers.deleteAll)

  // Routes Histoire du club
  .get('/histoire', HistoireClubControllers.getAll)
  .get('/histoire/:id', HistoireClubControllers.getId)
  .post('/histoire', HistoireClubControllers.post)
  .put('/histoire/:id', HistoireClubControllers.put)
  .delete('/histoire', HistoireClubControllers.deleteAll)
  .delete('/histoire/:id', HistoireClubControllers.deleteOne)

  // Routes Entraineur
  .get('/entraineurs', EntraineurControllers.getAll)
  .get('/entraineurs/:id', EntraineurControllers.getId)
  .post('/entraineurs', EntraineurControllers.post)
  .put('/entraineurs/:id', EntraineurControllers.put)
  .delete('/entraineurs', EntraineurControllers.deleteAll)
  .delete('/entraineurs/:id', EntraineurControllers.deleteOne)

  // Routes Activités
  .get('/activites', ActivitesControllers.getAll)
  .get('/activites/:id', ActivitesControllers.getId)
  .post('/activites', ActivitesControllers.post)
  .put('/activites/:id', ActivitesControllers.put)
  .delete('/activites', ActivitesControllers.deleteAll)
  .delete('/activites/:id', ActivitesControllers.deleteOne)
  .get('/activitesimages/', ActivitesImagesControllers.getAllImages)
  .post('/activitesimages/:id', upload.single('image'), sharpActivities, ActivitesImagesControllers.postImages)
  .delete('/activitesimages/:id', ActivitesImagesControllers.deleteImages)
  .delete('/activitesimages/', ActivitesImagesControllers.deleteAllImages)

  // Routes Calendrier
  .get('/calendrier', CalendrierControllers.getAll)
  .get('/calendrier/:id', CalendrierControllers.getId)
  .post('/calendrier', CalendrierControllers.post)
  .put('/calendrier/:id', CalendrierControllers.put)
  .delete('/calendrier/', CalendrierControllers.deleteAll)
  .delete('/calendrier/:id', CalendrierControllers.deleteOne)

  // Boutique
  .get('/store/', BoutiqueControllers.getAll)
  .get('/store/:id', BoutiqueControllers.getId)
  .post('/store', upload.single('image'), sharpBoutique, BoutiqueControllers.post)
  .put('/store/:id', upload.single('image'), sharpBoutique, BoutiqueControllers.put)
  .delete('/store/', BoutiqueControllers.deleteAll)
  .delete('/store/:id', BoutiqueControllers.deleteOne)

  // Routes Champions
  .get('/champions', ChampionsControllers.getAll)
  .get('/champions/:id', ChampionsControllers.getId)
  .post('/champions', upload.single('image'), sharpChampions, ChampionsControllers.post)
  .delete('/champions/', ChampionsControllers.deleteAll)
  .delete('/champions/:id', ChampionsControllers.deleteOne)

  // Routes Equipe Administrative
  .get('/equipe-administrative', EquipeAdministrative.getAll)
  .get('/equipe-administrative/:id', EquipeAdministrative.getId)
  .post('/equipe-administrative', EquipeAdministrative.post)
  .put('/equipe-administrative/:id', EquipeAdministrative.put)
  .delete('/equipe-administrative/', EquipeAdministrative.deleteAll)
  .delete('/equipe-administrative/:id', EquipeAdministrative.deleteOne)

  // Routes Inscription
  .get('/procedures', ProceduresControllers.getAll)
  .get('/procedures/:id', ProceduresControllers.getId)
  .post('/procedures', ProceduresControllers.post)
  .put('/procedures/:id', ProceduresControllers.put)
  .delete('/procedures/', ProceduresControllers.deleteAll)
  .delete('/procedures/:id', ProceduresControllers.deleteOne)

  // Routes Juges
  .get('/juges', JugesControllers.getAll)
  .get('/juges/:id', JugesControllers.getId)
  .post('/juges', JugesControllers.post)
  .put('/juges/:id', JugesControllers.put)
  .delete('/juges/', JugesControllers.deleteAll)
  .delete('/juges/:id', JugesControllers.deleteOne)

  // Routes Salle
  .get('/salle', SalleControllers.getAll)
  .get('/salle/:id', SalleControllers.getId)
  .post('/salle', upload.single('image'), sharpSalle, SalleControllers.post)
  .delete('/salle/', SalleControllers.deleteAll)
  .delete('/salle/:id', SalleControllers.deleteOne)

  // Routes ImageInscription
  .get('/imageinscription', ImageInscriptionControllers.getAll)
  .get('/imageinscription/:id', ImageInscriptionControllers.getId)
  .post('/imageinscription', upload.single('image'), sharpSalle, ImageInscriptionControllers.post)
  .delete('/imageinscription/:id', ImageInscriptionControllers.deleteOne)

  // Routes Tarifs
  .get('/tarifs', TarifsControllers.getAll)
  .get('/tarifs/:id', TarifsControllers.getId)
  .post('/tarifs', TarifsControllers.post)
  .put('/tarifs/:id', TarifsControllers.put)
  .delete('/tarifs/', TarifsControllers.deleteAll)
  .delete('/tarifs/:id', TarifsControllers.deleteOne)

  // Routes Years
  .get('/years', YearsControllers.getAll)
  .post('/years', YearsControllers.post)
  .put('/years/:id', YearsControllers.put)

  // Routes Link
  .get('/link', LinkControllers.getAll)
  .post('/link', LinkControllers.post)
  .put('/link/:id', LinkControllers.put)
  .delete('/link/', LinkControllers.deleteAll)
  .delete('/link/:id', LinkControllers.deleteOne)

module.exports = router;