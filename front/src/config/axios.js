import axios from "axios";

export const api = axios.create({
  baseURL: process.env.REACT_APP_SERVEUR,
  headers: { "X-Custom-Header": "VSF application" },
  timeout: 2000,
});