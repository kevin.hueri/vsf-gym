import React, { useState } from 'react';
import { BsFillArrowUpSquareFill } from 'react-icons/bs';
import styled from 'styled-components';

export const Button = styled.div`
   position: fixed; 
   width: 100%;
   left: 95%;
   bottom: 70px;
   height: 40px;
   font-size: 3rem;
   cursor: pointer;
   color: #5c5c5c;
   z-index: 1000;
`;

const ScrollButton = () => {

  const [visible, setVisible] = useState(false)

  const toggleVisible = () => {
    const scrolled = document.documentElement.scrollTop;
    if (scrolled > 300) { setVisible(true) }
    else if (scrolled <= 300) { setVisible(false) }
  };

  const scrollToTop = () => { window.scrollTo({ top: 0, behavior: 'smooth' }) };

  window.addEventListener('scroll', toggleVisible);

  return (
    <Button>
      <BsFillArrowUpSquareFill onClick={scrollToTop}
        style={{ display: visible ? 'inline' : 'none' }} />
    </Button>
  );
}

export default ScrollButton;