import React from 'react'
import { Box, Typography } from "@mui/material";
import Button from '@mui/material/Button';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';

function Footer() {
    const handleClickFacebook = () => { window.location.href = 'https://www.facebook.com/vsfgym' };
    const handleClickInstagram = () => { window.location.href = 'https://www.instagram.com/vsfgymnastique/?hl=fr' };
    return (
        <Box sx={{ bgcolor: '#292929', display: 'flex' }}>
            <Typography variant="body2" color="#fff" align='center' paddingY={2} sx={{ display: 'inline-block', width: '100%' }}>
                copyright@2022 VSF Gymnastique
            </Typography>
            <Button onClick={() => handleClickFacebook()} variant="h6">
                <FacebookIcon alt="Logo-facebook" fontSize='large' />
            </Button>
            <Button onClick={() => handleClickInstagram()} variant="h6">
                <InstagramIcon alt="Logo-facebook" fontSize='large' />
            </Button>
        </Box>
    )
}

export default Footer;
