import React, { useState, useEffect } from 'react';
import MainLayout from '../layouts/MainLayout';
import CssBaseline from "@mui/material/CssBaseline";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Footer from '../components/Footer';
import CardMedia from '@mui/material/CardMedia';
import { urlImg } from '../utils/url';

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/navigation";
import "swiper/css/thumbs";
import "swiper/css/grid";
import { FreeMode, Navigation, Thumbs } from "swiper";

import { useSelector, useDispatch } from "react-redux";
import { getStore } from '../store/actions/BoutiqueActions';

function ArticleCard({ step }) {
    return (
        <Box className='container'
            sx={{
                position: 'relative',
                m: 'auto',
                width: { xs: '350px', md: '600px' },
                height: '400px',
                overflow: 'hidden',
                background: '#fff',
                boxShadow: '5px 5px 15px rgba(0,0,0,0.5)',
                borderRadius: '10px'
            }}
        >
            <Box className='images'>
                <CardMedia component="img" image={`${urlImg + step.image}`} alt={step.name} sx={{ width: '290px', mt: '47px' }} />
            </Box>
            <Box className='product' sx={{ position: 'absolute', width: '40%', height: '100%', top: '10%', left: '60%' }}>
                <Typography variant="h2"
                    sx={{
                        fontSize: { xs: '1.5em', md: '3em' },
                        color: '#000', mt: '-5px', textAlign: 'left'
                    }}>{step.titre}</Typography>
                <Typography variant="h3"
                    sx={{
                        fontSize: { xs: '1.5em', md: '3em' },
                        color: '#C3A1A0', mt: '-5px', textAlign: 'left'
                    }}>{step.prix}€</Typography>
                <Typography variant='body1' className='desc'
                    sx={{
                        fontSize: { xs: '1em', md: '1.3em' },
                        color: '#000',
                        mb: '17px',
                        lineHeight: '1.6em',
                        mr: '25px'
                    }}
                >
                    {step.desc}
                </Typography>
            </Box>
        </Box>
    )
}

function ArticleCards({ step }) {
    return (
        <Box className='container'
            sx={{
                position: 'relative',
                m: ' 10px auto',
                width: { xs: '200px', md: '250px', lg: '300px' },
                overflow: 'hidden',
                background: '#fff',
                boxShadow: '5px 5px 15px rgba(0,0,0,0.5)',
                borderRadius: '10px'
            }}
        >
            <Box className='images'>
                <CardMedia component="img" image={`${urlImg + step.image}`} alt={step.name}
                    sx={{
                        width: { xs: '150px', md: '200px', lg: '290px' },
                        mt: '47px'
                    }}
                />
            </Box>
            <Box className='product' sx={{ position: 'absolute', width: '40%', height: '100%', top: '10%', left: '60%' }}>
                <Typography variant="h3"
                    sx={{
                        color: '#C3A1A0', mt: '-5px', textAlign: 'left',
                        fontSize: { xs: '1.5em', md: '3em' }
                    }}
                >{step.prix}€</Typography>
            </Box>
        </Box>
    )
}

export default function Boutique() {
    const dispatch = useDispatch();
    const store = useSelector((state) => state.boutique.boutiqueData);
    const [thumbsSwiper, setThumbsSwiper] = useState(null);

    useEffect(() => { dispatch(getStore()) }, [dispatch]);
    return (
        <MainLayout>
            <CssBaseline />

            {/* Titre */}
            <Box component="div" sx={{ pt: 5, padding: '85px 16px 24px' }}>
                <Typography variant='h2' align='center' sx={{ fontSize: { xs: "2.5em", md: "5em" }, my: 2 }} >
                    Boutique
                </ Typography>
            </Box>

            <Box component="div"
                sx={{
                    padding: { xs: '85px 16px 250px', sm: '85px 16px 100px',  md: '85px 16px 24px' }
                }}
            >

                {/* swiper */}
                <Swiper
                    style={{
                        "--swiper-navigation-color": "#fff",
                        "--swiper-pagination-color": "#fff",
                    }}
                    loop={true}
                    spaceBetween={10}
                    navigation={true}
                    thumbs={{ swiper: thumbsSwiper }}
                    modules={[FreeMode, Navigation, Thumbs]}
                    className="mySwiper2"
                >
                    {store.map((step, index) => (
                        <SwiperSlide key={index}>
                            <ArticleCard step={step} />
                        </SwiperSlide>
                    ))}
                </Swiper>
                <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                    <Swiper
                        onSwiper={setThumbsSwiper}
                        grid={{ rows: 10 }}
                        slidesPerView={3}
                        freeMode={true}
                        watchSlidesProgress={true}
                        modules={[FreeMode, Navigation, Thumbs]}
                        className="mySwiper"
                    >
                        {store.map((step, index) => (
                            <SwiperSlide key={index}>
                                <ArticleCards step={step} />
                            </SwiperSlide>
                        ))}
                    </Swiper>
                </Box>
            </Box>

            {/* Footer */}
            <Footer />

        </MainLayout >
    );
};