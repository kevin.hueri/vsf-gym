/*
 * Import - Module
 * *************** */
import { api } from "../../config/axios";

/*
 * Import types { ... }
 * ******************** */
import {
  GET_USER_DATA,
  ADD_USER_DATA,
  DELETE_USER_DATA,
  EDIT_USER_DATA,
} from "./ActionsTypes";

/*
 * Actions
 * ******* */

// Get User
export const getUser = () => {
  return (dispatch) => {
    return api
      .get("/user/")
      .then((res) => {
        dispatch({
          type: GET_USER_DATA,
          payload: res.data.dbUser
        });
      })
      .catch((err) => console.log(err));
  };
};

// Add User
export const addUser = (data) => {
  return (dispatch) => {
    return api
      .post("/user/", data)
      .then((res) => {
        dispatch({
          type: ADD_USER_DATA,
          payload: res.data.dbUser
        });
      })
      .catch((err) => console.log(err));
  };
};

// DeleteOne User
export const deleteUser = (id) => {
  return (dispatch) => {
    return api
      .delete(`/user/${id}`)
      .then((res) => {
        dispatch({
          type: DELETE_USER_DATA,
          payload: res.data.dbUser
        });
      })
      .catch((err) => console.log(err));
  };
}

// Edit User
export const editUser = (id) => {
  return (dispatch) => {
    return api
      .put(`/user/${id}`)
      .then((res) => {
        dispatch({
          type: EDIT_USER_DATA,
          payload: res.data.dbUser
        });
      })
      .catch((err) => console.log(err));
  };
}