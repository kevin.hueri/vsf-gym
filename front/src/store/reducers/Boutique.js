/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  boutiqueData: [],
};

/*
 * Reducers
 * ******** */
export function BoutiqueReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_STORE_DATA:
      return {
        ...state, boutiqueData: action.payload
      };
    case Actions.ADD_STORE_DATA:
      return {
        ...state, boutiqueData: action.payload
      };
    case Actions.DELETE_STORE_DATA:
      return {
        ...state, boutiqueData: action.payload, 
      };
      case Actions.EDIT_STORE_DATA:
        return {
          ...state, boutiqueData: action.payload, 
        };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
