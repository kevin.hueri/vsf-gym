/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  imageInscriptionData: [],
};

/*
 * Reducers
 * ******** */
export function ImageInscriptionReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_IMAGEINSCRIPTION_DATA:
      return {
        ...state, imageInscriptionData: action.payload
      };
    case Actions.ADD_IMAGEINSCRIPTION_DATA:
      return {
        ...state, imageInscriptionData: action.payload
      };
    case Actions.DELETE_IMAGEINSCRIPTION_DATA:
      return {
        ...state, imageInscriptionData: action.payload,
      };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
