/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  proceduresData: [],
};

/*
 * Reducers
 * ******** */
export function ProceduresReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_PROCEDURES_DATA:
      return {
        ...state, proceduresData: action.payload
      };
    case Actions.ADD_PROCEDURES_DATA:
      return {
        ...state, proceduresData: action.payload
      };
    case Actions.DELETE_PROCEDURES_DATA:
      return {
        ...state, proceduresData: action.payload, 
      };
      case Actions.EDIT_PROCEDURES_DATA:
        return {
          ...state, proceduresData: action.payload, 
        };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
