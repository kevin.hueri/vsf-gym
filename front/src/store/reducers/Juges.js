/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  jugesData: [],
};

/*
 * Reducers
 * ******** */
export function JugesReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_JUGES_DATA:
      return {
        ...state, jugesData: action.payload
      };
    case Actions.ADD_JUGES_DATA:
      return {
        ...state, jugesData: action.payload
      };
    case Actions.DELETE_JUGES_DATA:
      return {
        ...state, jugesData: action.payload, 
      };
      case Actions.EDIT_JUGES_DATA:
        return {
          ...state, jugesData: action.payload, 
        };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
