/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  calendrierData: [],
};

/*
 * Reducers
 * ******** */
export function CalendrierReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_CALENDRIER_DATA:
      return {
        ...state, calendrierData: action.payload
      };
    case Actions.ADD_CALENDRIER_DATA:
      return {
        ...state, calendrierData: action.payload
      };
    case Actions.DELETE_CALENDRIER_DATA:
      return {
        ...state, calendrierData: action.payload, 
      };
      case Actions.EDIT_CALENDRIER_DATA:
        return {
          ...state, calendrierData: action.payload, 
        };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
