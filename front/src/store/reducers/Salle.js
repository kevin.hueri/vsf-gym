/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  salleData: [],
};

/*
 * Reducers
 * ******** */
export function SalleReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_SALLE_DATA:
      return {
        ...state, salleData: action.payload
      };
    case Actions.ADD_SALLE_DATA:
      return {
        ...state, salleData: action.payload
      };
    case Actions.DELETE_SALLE_DATA:
      return {
        ...state, salleData: action.payload,
      };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
