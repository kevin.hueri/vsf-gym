/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  teamAdministrativeData: [],
};

/*
 * Reducers
 * ******** */
export function EquipeAdministrativeReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_EQUIPEADMINISTRATIVE_DATA:
      return {
        ...state, teamAdministrativeData: action.payload
      };
    case Actions.ADD_EQUIPEADMINISTRATIVE_DATA:
      return {
        ...state, teamAdministrativeData: action.payload
      };
    case Actions.DELETE_EQUIPEADMINISTRATIVE_DATA:
      return {
        ...state, teamAdministrativeData: action.payload, 
      };
      case Actions.EDIT_EQUIPEADMINISTRATIVE_DATA:
        return {
          ...state, teamAdministrativeData: action.payload, 
        };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
