/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  histoireClubData: [],
};

/*
 * Reducers
 * ******** */
export function HistoireClubReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_HISTOIRECLUB_DATA:
      return {
        ...state, histoireClubData: action.payload
      };
    case Actions.ADD_HISTOIRECLUB_DATA:
      return {
        ...state, histoireClubData: action.payload
      };
    case Actions.DELETE_HISTOIRECLUB_DATA:
      return {
        ...state, histoireClubData: action.payload, 
      };
      case Actions.EDIT_HISTOIRECLUB_DATA:
        return {
          ...state, histoireClubData: action.payload, 
        };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
