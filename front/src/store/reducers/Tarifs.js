/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  tarifsData: [],
};

/*
 * Reducers
 * ******** */
export function TarifsReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_TARIFS_DATA:
      return {
        ...state, tarifsData: action.payload
      };
    case Actions.ADD_TARIFS_DATA:
      return {
        ...state, tarifsData: action.payload
      };
    case Actions.DELETE_TARIFS_DATA:
      return {
        ...state, tarifsData: action.payload, 
      };
      case Actions.EDIT_TARIFS_DATA:
        return {
          ...state, tarifsData: action.payload, 
        };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
