/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  championsData: [],
};

/*
 * Reducers
 * ******** */
export function ChampionsReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_CHAMPIONS_DATA:
      return {
        ...state, championsData: action.payload
      };
    case Actions.ADD_CHAMPIONS_DATA:
      return {
        ...state, championsData: action.payload
      };
    case Actions.DELETE_CHAMPIONS_DATA:
      return {
        ...state, championsData: action.payload, 
      };
      case Actions.EDIT_CHAMPIONS_DATA:
        return {
          ...state, championsData: action.payload, 
        };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
