/*
 * Import Actions { ... }
 * ********************** */
import * as Actions from "../actions/ActionsTypes";

/*
 * Selector
 * ******** */
const initialState = {
  yearsData: [],
};

/*
 * Reducers
 * ******** */
export function YearsInscriptionReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_YEARSINSCRIPTION_DATA:
      return {
        ...state, yearsData: action.payload
      };
    case Actions.ADD_YEARSINSCRIPTION_DATA:
      return {
        ...state, yearsData: action.payload
      };
    case Actions.EDIT_YEARSINSCRIPTION_DATA:
      return {
        ...state, yearsData: action.payload,
      };
    default:
      return state;
  }
}

/*
 * Getters
 * ******* */
